class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class=container render>
    <div class="card p-4 mb-5">
    <img class="mb-5" id="gambar" src="${this.image}"/>
    <h5 >${this.manufacture} ${this.model}</h5>
    <h4 class="harga"> Rp ${this.rentPerDay} / hari </h4>
    <p class="elipsis"> ${this.description}</p>
    <p><i class="bi bi-people me-2"></i>${this.capacity} orang </p>
    <p><i class="bi bi-gear me-2"></i>${this.transmission} </p>
    <p><i class="bi bi-calendar4 me-2"></i>Tahun ${this.year}</p>
    <button class="btn btn-success fw-bold">Pilih Mobil</button>
    </div>
</div>    `;
  }
}