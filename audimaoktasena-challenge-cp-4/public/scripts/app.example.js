class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.tipeDriver = document.getElementById('tipeDriver');
    this.date = document.getElementById('date');
    this.waktuJemput = document.getElementById('waktuJemput');
    this.penumpang = document.getElementById('penumpang');
    this.cariMobil = document.getElementById('cariMobil');
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    // this.loadButton.onclick = this.run;
    this.cariMobil.addEventListener('click', () => {
      this.filter();
    })
  }


  filter = () => {
    this.driver = this.tipeDriver.value
    this.jumlahPenumpang = this.penumpang.value
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }


    Car.list.forEach((car) => {
      if (this.driver == car.available) {
        const node = document.createElement("div");
        node.className = "col-lg-4"
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
      if (this.jumlahPenumpang == car.capacity) {
        const node = document.createElement("div");
        node.className = "col-lg-4"
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
    })

  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className = "col-lg-4"
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}